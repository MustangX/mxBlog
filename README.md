# mxBlog

is the name of the project we are building utilizing the Laravel Framework. 

If things work out, we may use it as a base for our main project, [**MustangX CMS**](http://mustangx.org).

## Issues, bugs, complaints, sugeestions...
that and more @ the [Issues Area](/issues)


## Development Documentation
some @ the [Dev Wiki](/wikis/home)

and some on the [HubShark site](http://hubshark.com)